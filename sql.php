<html>
    <head>
        <?php
            require_once('connection.php') 
        ?>
    </head>
    <body>
        <?php
            if(isset($_POST['obrok'])){
                if ($_POST['obrok']!='List') {
                    $meal=$_POST['obrok'];
                    $Date=$_POST['selected_date'];
                    echo '<p class="p1">Vrsta hrane</p>';
                    echo ' <input list="ice-cream-flavors" id="ice-cream-choice" name="ice-cream-choice" class="lista"/>';
                    echo  '<datalist id="ice-cream-flavors">';
                        echo'<option value="Chocolate">';
                        echo'<option value="Coconut">';
                        echo '<option value="Mint">';
                        echo '<option value="Strawberry">';
                        echo '<option value="Vanilla">';
                    echo '</datalist>';
                    echo '<p class="p2">Kolicina hrane</p>';
                    echo '<input type="text" id="kolicina" name="kolicina">';
                    echo '<select id="mera">';
                       echo '<option value="Kg">Kg</option>';
                       echo '<option value="L">L</option>';
                       echo '<option value="Kom">Kom</option>'; 
                    echo'</select>';
                    echo '<button id="addnewinput" onclick="NewInput()">+</button>';
                    echo'<br>';
                    echo '<div id="rew"></div>';
                    echo "<button onclick='insert(\"$Date\",\"$meal\")'>Unesi</button>";
                }elseif($_POST['obrok']=='List'){
                    $Date=$_POST['selected_date'];
                  echo writeTable($Date);
                }else{
                    echo "Greska";
                }
            }
            if(isset($_POST['Meal'])){
                if ($_POST['Meal']!='List') {
                    $mera=$_POST['mera'];
                    $obrok=$_POST['Meal'];
                    if ($obrok=="Breakfast") {
                       $obrok=1;
                    }elseif ($obrok=="Lunch") {
                       $obrok=2;
                    }elseif($obrok=="Dinner"){
                       $obrok=3;
                    }else{
                        $obrok="";
                    }
                    $datum=$_POST['selected_date'];
                    //echo $datum;
                    $str= $_POST['kolicina_hrane'];
                    $kolicina_hrane=explode(",",$str);
                    $str2= $_POST['vrsta_hrane'];
                    $vrsta_hrane=explode(",",$str2);
                    $array3=array_combine($vrsta_hrane, $kolicina_hrane);
                    foreach($array3 as $x => $val) {
                        $inset= "('".$x."' ,$val,'".$mera."',$obrok,'".$datum."')";
                        $sql="INSERT INTO `obroci`( `vrsta hrane`, `kolicina hrane`,`jedinica mere`,`obrok`,`datum`) VALUES $inset" ;
                        $result2=$con->query($sql);
                        // echo $sql;
                    }
                }else{
                     echo $_POST['Meal'];
                }
            }
            if(isset($_POST['Delete'])){
                $id_Delete=$_POST['selected_id'];
                $Delete="DELETE FROM `obroci` WHERE id='". $id_Delete."'";
                $result=$con->query($Delete);
                $Date=$_POST['selected_date'];
                echo writeTable($Date);
            }
            if (isset($_POST['id_Change'])) {
                $Vrs_H=$_POST['Vrs_H'];
                $Kol_H=$_POST['Kol_H'];
                $Jed_M=$_POST['Jed_M'];
                $id_Change=$_POST['id_Change'];
                $Date=$_POST['datum'];
                 $sql_update="UPDATE `obroci` SET `vrsta hrane`='".$Vrs_H."',`kolicina hrane`='".$Kol_H."',`jedinica mere`='".$Jed_M."' WHERE `id`='".$id_Change."' ";
                $result=$con->query($sql_update);
                echo writeTable($Date);
            }
            function writeTable($Date) {
                echo "<table id='customers'>";
                    echo "<tr><th>";
                        echo "Vrsta hrane";
                    echo "</th><th>";
                        echo "Kolicina hrane";
                    echo "</th><th>";
                        echo "Kol";
                    echo "</th><th>";
                    echo "</th></tr>";    
                    $sql="SELECT*, (SELECT COUNT(*) FROM `obroci` WHERE datum='".$Date."')as kolone FROM `obroci` WHERE datum='".$Date."' ";
                    $result=$GLOBALS['con']->query($sql);
                    while ($row = $result->fetch_assoc()) {
                        $vrsta_hrane=$row['vrsta hrane'];
                        $kolicina_hrane=$row['kolicina hrane'];
                        $jedinica_mere= $row['jedinica mere'];
                        $kolona=$row['kolone'];
                        $Vrs='vrsta_hrane';
                        $kol='kolicina_hrane';
                        $jed='jedinica mere';
                        $id=$row['id'];
                        echo " <tbody>";
                            echo "<tr id='tr".$id."'>
                                <td id='vrsta_hrane".$id."' onclick='update(\"$id\",\"$vrsta_hrane\",\"$kolicina_hrane\",\"$jedinica_mere\",\"$Vrs\")'>";
                                echo $row['vrsta hrane'];
                            echo "</td><td  id='kolicina_hrane".$id."' onclick='update(\"$id\",\"$vrsta_hrane\",\"$kolicina_hrane\",\"$jedinica_mere\",\"$kol\")'>"; 
                                 echo $row['kolicina hrane'];
                            echo "</td><td id='jedinica mere".$id."' onclick='update(\"$id\",\"$vrsta_hrane\",\"$kolicina_hrane\",\"$jedinica_mere\",\"$jed\")'>"; 
                                echo $row['jedinica mere'];
                            echo "</td><td>";
                                echo "<button onclick='Del(\"$id\",\"$Date\")'>Obrisi</button>";
                            echo "</td></tr>"; 
                        echo " </tbody>"; 
                    }
                echo "</table>";
            }
        ?>
    </body>    
</html>