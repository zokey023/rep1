const date = new Date();
const renderCalendar = () => {
  date.setDate(1);
  const monthDays = document.querySelector(".days");
  const lastDay = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDate();
  const prevLastDay = new Date(
    date.getFullYear(),
    date.getMonth(),
    0
  ).getDate();
  const firstDayIndex = date.getDay();
  const lastDayIndex = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDay();
  const nextDays = 7 - lastDayIndex - 1;
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  //console.log(months.length)
  document.querySelector(".date h1").innerHTML = months[date.getMonth()];
  document.querySelector(".date p").innerHTML = new Date().toDateString();
  let days = "";
  for (let x = firstDayIndex; x > 0; x--) {
    days += `<div class="prev-date">${prevLastDay - x + 1}</div>`;
  }
  for (let i = 1; i <= lastDay; i++) {
    var meseci= months[date.getMonth()];
    let day;
    switch (meseci) {
      case "January":
      meseci = "1";
      break;
      case "February":
      meseci = "2";
      break;
      case "March":
      meseci = "3";
      break;
      case "April":
      meseci = "4";
      break;
      case "May":
      meseci = "5";
      break;
      case "June":
      meseci = "6";
      break;
      case  "July":
      meseci = "7";
      break;
      case "August":
      meseci="8"; 
      break;
      case "September":
      meseci="9";
      break;
      case "October":
      meseci="10";
      break;
      case "November":
      meseci="11";
      break;
      case "December":
      meseci="12";    
    }
    var year=date.getFullYear();
    if (i === new Date().getDate() &&date.getMonth() === new Date().getMonth()) {
      days += `<div class="today" onclick='modal()'>${i}</div>`;
    } else {
      days += `<div  id="myBtn" class='DANI'onclick='modal(\"${year}\",\"${meseci}\",\"${i}\")'>${i}</div>`;
    } 
  }
  for (let j = 1; j <= nextDays; j++) {
    days += `<div class="next-date">${j}</div>`;
    monthDays.innerHTML = days;
  }
};
document.querySelector(".prev").addEventListener("click", () => {
  date.setMonth(date.getMonth() - 1);
  renderCalendar();
});
document.querySelector(".next").addEventListener("click", () => {
  date.setMonth(date.getMonth() + 1);
  renderCalendar();
});
renderCalendar();
function modal(meseci,abvg,year) {
  document.getElementById("p1").innerHTML =meseci+"-"+abvg+"-"+ year;
  console.log(meseci,abvg,year)
  var modal = document.getElementById("myModal");
  modal.style.display = "block";
  var selected_date=document.getElementById("p1").textContent;
  jQuery.ajax({
    url: 'sql.php',
    method: 'POST',
    data: 'obrok=List' + '&selected_date='+selected_date,
    success: function(response) {
      $('#ajax').html(response);
    }
  });
}
var span=document.getElementsByClassName("close")[0];
span.onclick = function() {
  var modal = document.getElementById("myModal");  
  modal.style.display = "none";
  document.getElementById("jedan").className = "btn active"; 
  document.getElementById("dva").className = "btn"; 
  document.getElementById("tri").className = "btn"; 
  document.getElementById("cetiri").className = "btn"; 
}
window.onclick = function(event) {
  var modal = document.getElementById("myModal");
  if (event.target == modal) {
    modal.style.display = "none";
    document.getElementById("jedan").className = "btn active"; 
    document.getElementById("dva").className = "btn"; 
    document.getElementById("tri").className = "btn"; 
    document.getElementById("cetiri").className = "btn";      
  }
}
function meal(bbb){
  var meal=bbb;
  var selected_date=document.getElementById("p1").textContent;
  // console.log(selected_date)
  jQuery.ajax({
    url: 'sql.php',
    method: 'POST',
    data: 'obrok='+meal + '&selected_date='+selected_date,
    success: function(response) {
      $('#ajax').html(response);
    }
  });
}
var header = document.getElementById("btnss");
var btns = header.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
} 
function NewInput(){
  var h = document.getElementById("rew");
  h.insertAdjacentHTML("afterend", "<input list='ice-cream-flavors' id='ice-cream-choice' name='ice-cream-choice' class='lista'/><input type='text' id='kolicina' name='kolicina'><select id='mera'><option value='Kg'>Kg</option><option value='L'>L</option><option value='Kom'>Kom</option></select><br>");
}
function insert(datum,meal){
  var lols = document.getElementsByName("ice-cream-choice");
  var vals=[];
  for (var i=0, n=lols.length;i<n;i++) {
    vals.push(lols[i].value);
  }
  console.log(vals);
  var kolicina_hrane=document.getElementsByName("kolicina");
  var kol_niz=[];
  for(var a=0,b=kolicina_hrane.length;a<b;a++ ){
    //console.log(b)
    // console.log(kolicina_hrane[a].value)
    kol_niz.push(kolicina_hrane[a].value);
  }
   //console.log(kol_niz)
  var mera = document.getElementById("mera").value;
  var selected_date = datum;
  let text = "Dali ste sigurni da zelite da unesete?";
  if (confirm(text) == true) {
    jQuery.ajax({
      url: 'sql.php',
      method: 'POST',
      data: 'Meal='+meal +
      '&selected_date='+selected_date+
      '&br_kolona='+b+
      '&vrsta_hrane='+vals+
      '&kolicina_hrane='+kol_niz+
      '&mera='+mera,
      success: function(response) {
        $('#ajax').html(response);
      }
    });
    jQuery.ajax({
      url: 'sql.php',
      method: 'POST',
      data: 'obrok='+meal+
      '&selected_date='+selected_date,
      success: function(response) {
        $('#ajax').html(response);
      }
    });
  }
}
function Del(id,date) {
  var row=document.getElementById('tr'+id);
  var del='del';
  let text = "Dali ste sigurni da zelite da obrisete?";
  if (confirm(text) == true) {
    jQuery.ajax({
      url: 'sql.php',
      method: 'POST',
      data: 'Delete='+del+
      '&selected_id='+id+
      '&selected_date='+date,
      success: function(response) {
        $('#ajax').html(response);
        row.style.display='block';
      }
    });
  }
}
function update(id,vrsH,kolH,jedM,vrs,kol,jed) {
  var row2=document.getElementById('tr'+id);
  var new_row = document.createElement('tr');
  new_row.innerHTML=`<td><input type='text' id="vrsta" value="`+vrsH+`"></td><td><input type='text' id="kolicina" value="`+kolH+`"></td><td><input type='text' id="jedinica"  value="`+jedM+`"></td><td><button onclick='change(\"${id}\")'>Promeni</button></td>`;
  row2.after(new_row);
}
function change(a){
  var id=a;
  var selDate=document.getElementById("p1").textContent;
  var vrsta_hrane=document.getElementById('vrsta').value;
  var kolicina_hrane=document.getElementById('kolicina').value;
  var jedinica_mere=document.getElementById('jedinica').value;
  console.log(vrsta_hrane,kolicina_hrane,jedinica_mere);
  jQuery.ajax({
      url: 'sql.php',
      method: 'POST',
      data: 'Vrs_H='+vrsta_hrane+
      '&Kol_H='+kolicina_hrane+
      '&Jed_M='+jedinica_mere+
      '&id_Change='+id+
      '&datum='+selDate,
      success: function(response) {
        $('#ajax').html(response);
      }
    });
}