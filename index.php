<?php
  require_once('connection.php') 
?>
<html>
  <head>
    <style>
      <?php
       require_once('style.css');
      ?>
    </style>
    
<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Calendar</title>
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"
    />
  </head>
  <body>
    <div class="container">
      <div class="calendar">
        <div class="month">
          <i class="fas fa-angle-left prev"></i>
          <div class="date">
            <h1></h1>
            <p></p>
          </div>
          <i class="fas fa-angle-right next"></i>
        </div>
        <div class="weekdays">
          <div>Sun</div>
          <div>Mon</div>
          <div>Tue</div>
          <div>Wed</div>
          <div>Thu</div>
          <div>Fri</div>
          <div>Sat</div>
        </div>
        <div class="days"></div>
      </div>
    </div>
    <div id="myModal" class="modal">
      <div class="modal-content">
        <div class="modal-header">
          <span class="close">&times;</span>
          <h3 id="p1"></h3>
        </div>
        <div class="modal-body">
          <div  id="btnss"class="topnav">
            <a id="jedan" class="btn active" onclick="meal('List')" href="#List">List</a>
            <a id="dva" class="btn" onclick="meal('Breakfast')" href="#Breakfasti">Breakfast</a>
            <a id="tri"  class="btn" onclick="meal('Lunch')" href="#Lunch">Lunch</a>
            <a id="cetiri"  class="btn" onclick="meal('Dinner')" href="#Dinner">Dinner</a>
          </div>
          <div id="ajax"></div>
        </div>
        <div class="modal-footer">
          <h3>Modal Footer</h3>
        </div>
      </div>
    </div>
    <script src="script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </body>
</html>
